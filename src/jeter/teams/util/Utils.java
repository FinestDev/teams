package jeter.teams.util;

import java.util.ArrayList;
import java.util.List;

import jeter.teams.Teams;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Utils {

	public static List<Team> teams = new ArrayList<Team>();
	
	Teams pl;
	public Utils(Teams in){
		pl = in;
	}

	public static Team getTeamByName(String name){
		for(Team t : teams){
			if(t.getName().toLowerCase().equals(name.toLowerCase())){
				return t;
			}
		}
		return null;
	}
	
	public static Team getPlayerTeam(Player p){
		for(Team t : teams){
			if(t.getMembers().contains(p.getName())){
				return t;
			}
		}
		return null;
	}

	public static void createTeam(Player p, String name){
		
		if(getPlayerTeam(p) != null){ p.sendMessage("�cYou must leave your current team first!"); return; }
		if(getTeamByName(name) != null){ p.sendMessage("�cThis team already exists!"); return; }
		
		Team t = new Team(name, p.getName());
		t.addMember(p);
		p.sendMessage("�aYou have created team "+name);
		
	}
	
	public void invite(final Player p, Team team){
		if(team.getPending().contains(p.getName())){ return; }
		team.addPending(p.getName());

		p.sendMessage("�cYou've been invited to team "+team.getName()+". Join with /team join "+team.getName()+". You have 15 seconds!");
		
		for(String s : team.getMembers()){
			if(Bukkit.getPlayer(s).isOnline()){
				Bukkit.getPlayer(s).sendMessage("�a"+p.getName()+" has been invited to your team!");
			}
		}
		
		final String name = team.getName();
		Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable(){
			
			public void run(){
				
				Team t = getTeamByName(name);
				if(t.getPending().contains(p.getName())){
					t.removePending(p.getName());
					p.sendMessage("�cYour team invite has expired");
					if(t.getOwner().isOnline()){
						t.getOwner().sendMessage(p.getName()+"'s invite has expired");
					}
				}
				
			}
			
		}, 20L * 15);
	}
	
	public static void join(Player p, String name){
		if(getTeamByName(name) == null){ p.sendMessage("�cNo such team exists!"); return; }
		if(getPlayerTeam(p) != null){ p.sendMessage("�cYou're already part of a team!"); return; }
		
		Team team = getTeamByName(name);
		if(!team.getPending().contains(p.getName())){ p.sendMessage("�cYou have not been invited to this team!"); return; }
		
		for(String s : team.getMembers()){
			if(Bukkit.getPlayer(s).isOnline()){
				Bukkit.getPlayer(s).sendMessage("�a"+p.getName()+" has joined your team!");
			}
		}
		
		team.removePending(p.getName());
		team.addMember(p);
		p.sendMessage("�aYou have joined team "+name);
		
	}
	
	public static void kick(Player p, Team t){
		if(t.getMembers().contains(p.getName())){
			
			t.removeMember(p);
			for(String s : t.getMembers()){
				if(Bukkit.getPlayer(s).isOnline()){
					Bukkit.getPlayer(s).sendMessage("�a"+p.getName()+" has been kicked from your team!");
				}
			}
			
			p.sendMessage("�4You have been kicked from team "+t.getName());
			
			
		}
	}

}
