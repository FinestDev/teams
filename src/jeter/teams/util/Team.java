package jeter.teams.util;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Team {
	
	String name;
	String owner;
	List<String> members;
	List<String> pending;
	
	public Team(String teamName, String owner) {
		
		this.name = teamName;
		this.owner = owner;
		this.members = new ArrayList<String>();
		this.pending = new ArrayList<String>();
		
		Utils.teams.add(this);
		
	}
	
	public Player getOwner(){
		return Bukkit.getPlayer(owner);
	}
	
	public void setOwner(String owner){
		this.owner = owner;
	}
	
	public String getName(){
		return this.name;
	}
	
	public List<String> getMembers(){
		return this.members;
	}
	
	public List<String> getPending(){
		return this.pending;
	}
	
	public void addPending(String s){
		this.pending.add(s);
	}
	
	public void removePending(String s){
		this.pending.remove(s);
	}
	
	public void addMember(Player p) {
		this.members.add(p.getName());
	}
	
	public void removeMember(Player p){
		this.members.remove(p.getName());
	}

}
