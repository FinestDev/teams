package jeter.teams;


import java.util.Iterator;

import jeter.teams.util.Team;
import jeter.teams.util.Utils;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.Main;
import org.bukkit.entity.Player;

public class Commands implements CommandExecutor{
	
	Teams pl;
	public Commands(Teams in){
		pl = in;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		if(sender instanceof Player){ // It's a player
			Player p = (Player) sender;
		
			if(cmd.getName().equals("team")){
				if(args.length == 2){
					
					if(args[0].equals("create")){
						
						Utils.createTeam(p, args[1]);
					
					}else
					if(args[0].equals("join")){
						
						Utils.join(p, args[1]);
						
					}else
					if(args[0].equals("invite")){
						
						Team t = Utils.getPlayerTeam(p);
						if(Utils.getPlayerTeam(p) == null){ p.sendMessage("�cYou're not part of a team!"); return false; }
						if(!(t.getOwner().getName().equals(p.getName()))){ p.sendMessage("�cYou do not own this team!"); return false; }
						
						Player invited = null;
						
						if(Bukkit.getPlayer(args[1]) == null){ p.sendMessage("�cNo such player exists."); return false; }
						if(Bukkit.getPlayer(args[1]).isOnline()){
							invited = Bukkit.getPlayer(args[1]);
						}
						
						if(invited == null){ p.sendMessage("�cNo such player exists."); return false; }
						
						if(t.getPending().contains(invited.getName())){	p.sendMessage("�cThat player has already been invited"); return false;}
						if(t.getMembers().contains(invited.getName())){ p.sendMessage("�cThis player is already a member of your team"); return false; }
						
						pl.util.invite(invited, t);
						
					}else
					if(args[0].equals("kick")){
						
						Team t = Utils.getPlayerTeam(p);
						if(Utils.getPlayerTeam(p) == null){ p.sendMessage("�cYou're not part of a team!"); return false; }
						if(!(t.getOwner().getName().equals(p.getName()))){ p.sendMessage("�cYou do not own this team!"); return false; }
						
						Player invited = null;
						if(Bukkit.getPlayer(args[1]) == null){ p.sendMessage("�cNo such player exists."); return false; }
						if(Bukkit.getPlayer(args[1]).isOnline()){
							invited = Bukkit.getPlayer(args[1]);
						}
						
						if(invited == null){ p.sendMessage("�cNo such player exists."); return false; }
						
						if(t.getOwner().getName().equals(invited.getName())){ p.sendMessage("�cYou cannot kick yourself from the team."); return false; }
						
						if(t.getMembers().contains(invited.getName())){
							Utils.kick(invited, t);
						}
						
						
					}else
					if(args[0].equals("leave")){
						
						Team t = Utils.getPlayerTeam(p);
						if(Utils.getPlayerTeam(p) == null){ p.sendMessage("�cYou're not part of a team!"); return false; }
						if(!t.getName().equals(args[1])){ p.sendMessage("�cYou're not a part of team "+t.getName()); return false; }
						
						if(t.getOwner().getName().equals(p.getName())){
							Iterator<String> it = t.getMembers().iterator();
							
							while(it.hasNext()){
								String player = it.next();
								if(Bukkit.getPlayer(player).isOnline()){
									Bukkit.getPlayer(player).sendMessage("�cYour team has been disbanded!");
									it.remove();
								}
							}
							
							t.getPending().clear();
							Utils.teams.remove(t);
						}else{
							t.removeMember(p);
							for(String s : t.getMembers()){
								if(Bukkit.getPlayer(s).isOnline()){
									Bukkit.getPlayer(s).sendMessage("�a"+p.getName()+" has left the team!");
								}
							}
						}
						
					}
					
				}else{
					p.sendMessage("�b/team <create>");
					p.sendMessage("�b/team join <team>");
					p.sendMessage("�b/team leave <team>");
					p.sendMessage("�b/team invite <player>");
					p.sendMessage("�b/team kick <player>");
				}
			}
			
		}
		
		return false;
	}

}
