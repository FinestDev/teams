package jeter.teams.events;


import jeter.teams.util.Utils;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class Events implements Listener {
	
	@EventHandler
	public void onDMG(EntityDamageByEntityEvent e){
		
		if(e.getEntity() instanceof Player && e.getDamager() instanceof Player){
			
			Player hit = (Player) e.getEntity();
			Player damager = (Player) e.getDamager();
			
			if(Utils.getPlayerTeam(hit).getName().equals(Utils.getPlayerTeam(damager).getName())){
				e.setCancelled(true);
			}
			
		}
		
	}
}
