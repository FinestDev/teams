package jeter.teams;

import jeter.teams.events.Events;
import jeter.teams.util.Utils;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Teams extends JavaPlugin {
	
	Utils util = new Utils(this);

	public void onEnable() {
		
		Bukkit.getPluginManager().registerEvents(new Events(), this);
		getCommand("team").setExecutor(new Commands(this));
	}


}
